<?php

/**
 * Plugin Name: BravoWP Image Optimizer
 * License: GPL2+
 */

if(!defined('ABSPATH')) {
  exit;
}

if(!class_exists('BravoWP_Image_Optimizer_Plugin')) {
  class BravoWP_Image_Optimizer_Plugin {

    public static function getFiles() {
      return array(
        'bravo-image-proxy.php' => '<?php
          require \'$[PLUGIN_DIR]bravo-wp-image-optimizer/image.php\';
          $optimizer = new BravoWP_Image_Optimizer(__DIR__);
          $optimizer->run();
        ',
        '.htaccess' => '
          <IfModule mod_rewrite.c>
            RewriteEngine on
            RewriteBase $[UPLOADS_URL]

            # try cache first
            RewriteCond %{QUERY_STRING} !original
            RewriteCond %{DOCUMENT_ROOT}$[UPLOADS_URL]$[CACHE_FOLDER]%{REQUEST_URI} -f
            RewriteRule . $[UPLOADS_URL]$[CACHE_FOLDER]%{REQUEST_URI} [L]

            # to php
            RewriteCond %{REQUEST_FILENAME} !/$[CACHE_FOLDER]
            RewriteCond %{REQUEST_FILENAME} !.php
            RewriteCond %{REQUEST_FILENAME} \.jpg$
            RewriteCond %{REQUEST_FILENAME} -f
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{QUERY_STRING} !original
            RewriteRule . $[UPLOADS_URL]bravo-image-proxy.php [QSA,L]
          </IfModule>
        ',
      );
    }

    public static function getUploadsDir() {
      return WP_CONTENT_DIR . '/uploads/';
    }

    public static function getUploadsUrl() {
      $uploads_url = parse_url(WP_CONTENT_URL, PHP_URL_PATH);
      $uploads_url = rtrim($uploads_url, '/') . '/uploads/';
      return $uploads_url;
    }

    public static function getCacheFolder() {
      return 'bravo-image-cache/';
    }

    public static function getCacheDir() {
      return static::getUploadsDir() . static::getCacheFolder();
    }

    public static function getCacheUrl() {
      return static::getUploadsUrl() . static::getCacheFolder();
    }

    public static function isSupporedEnvironment() {
      return isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Apache') !== false;
    }

    public static function isActivated() {
      $files = static::getFiles();
      foreach($files as $file => $value) {
        $filename = static::getUploadsDir() . $file;
        if(!is_file($filename)) {
          return false;
        }
      }
      if(!is_dir(static::getCacheDir())) {
        return false;
      }
      if(!is_file(__DIR__ . '/vendor/autoload.php')) {
        return false;
      }
      return true;
    }

    public static function init() {
      register_activation_hook(__FILE__, array(__CLASS__, 'activate'));
      register_deactivation_hook(__FILE__, array(__CLASS__, 'deactivate'));

      add_action('admin_menu', function() {
        $menu = 'bravo-image-optimizer';
        $title = 'Image optimizer';
        $notes = 0;
        if(!BravoWP_Image_Optimizer_Plugin::isActivated()) {
          $notes++;
        }
        if($notes) {
          $title .= sprintf('<span class="awaiting-mod">%d</span>', 1);
        }
        add_menu_page('Image optimizer', $title, 'manage_options', $menu, function() {
          require(__DIR__ . '/admin.php');
        });
        add_submenu_page($menu, 'Image optimizer', 'Statistics', 'manage_options', 'submenu-stats', function() {
          require(__DIR__ . '/admin_stats.php');
        });
      });
    }

    public static function activate() {
      if(!static::isSupporedEnvironment()) {
        echo 'Error: Unsupported environment (apache only for now :()';
        die;
      }
      $files = static::getFiles();
      foreach($files as $file => $value) {
        $value = strtr($value, array(
          '$[PLUGIN_DIR]' => '../plugins/',
          '$[UPLOADS_URL]' => static::getUploadsUrl(),
          '$[CACHE_FOLDER]' => static::getCacheFolder(),
        ));
        $filename = static::getUploadsDir() . $file;
        if(!file_put_contents($filename, $value)) {
          echo "Error writing $filename";
          die;
        }
      }
    }

    public static function deactivate() {
      $files = static::getFiles();
      foreach($files as $file => $value) {
        $filename = static::getUploadsDir() . $file;
        if(!unlink($filename)) {
          echo "Error removing $filename";
        }
      }
    }
  }

  BravoWP_Image_Optimizer_Plugin::init();
}
