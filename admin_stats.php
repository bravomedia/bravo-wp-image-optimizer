<?php

if(!defined('ABSPATH')) {
  exit;
}

$limit = 20;
$cache_dir = BravoWP_Image_Optimizer_Plugin::getCacheDir();
$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($cache_dir));

if(isset($_GET['filter'])) {
  /* $iterator = new RecursiveCallbackFilterIterator($iterator, function($current, $key, $iterator) {
    print_r((string)$current)
    if($current->isFile() && strpos((string)$current, $_GET['filter']) === false) {
      return false;
    }
    return true;
  }); */
  $regex = '/(.*)(' . preg_quote($_GET['filter']) . ')(.*)/i';
  $iterator = new RegexIterator($iterator, $regex);
}

echo "<style>
.bravo-stats-table th {
  text-align: left;
}
.bravo-stats-table th,
.bravo-stats-table td {
  padding: 5px 10px;
  white-space: nowrap;
}
</style>";

echo "<br>";

echo "<form>";
echo "<input name='filter' value='" . (isset($_GET['filter']) ? htmlspecialchars($_GET['filter']) : '') . "'>";
echo "<input type='submit' value='Filter'>";
echo "<input type='hidden' name='page' value='" . $_GET['page'] ."'>";
echo "</form>";

echo "<table width='100%' class='bravo-stats-table'>";
echo "<tr>
    <th width='1'>Filename</th>
    <th width='1'>Original</th>
    <th width='1'>Optimized</th>
    <th>Compression ratio</th>
  </tr>";

foreach($iterator as $node) {
  if($node->isFile()) {
    $file = (string)$node;
    $orig = str_replace(BravoWP_Image_Optimizer_Plugin::getUploadsUrl() . BravoWP_Image_Optimizer_Plugin::getCacheFolder(), '/', $file);
    if(!is_file($orig)) {
      continue;
    }
    $relativeName = str_replace(BravoWP_Image_Optimizer_Plugin::getUploadsDir(), '', $orig);
    $origUrl = BravoWP_Image_Optimizer_Plugin::getUploadsUrl() . $relativeName . '?original';
    $fileUrl = BravoWP_Image_Optimizer_Plugin::getUploadsUrl() . $relativeName;
    $fileSize = filesize($file);
    $origSize = filesize($orig);
    $compression = ($origSize - $fileSize) / $origSize * 100;
    printf("<tr>
      <td>%s</td>
      <td><a href='%s' target='_blank'>%.01f kB</a></td>
      <td><a href='%s' target='_blank'>%.01f kB</a></td>
      <td>%.01f%%</td>
    </tr>", basename($file), $origUrl, $origSize / 1000, $fileUrl, $fileSize / 1000, $compression);
    if($limit && --$limit < 1) {
      break;
    }
  }
}

echo "</table>";
