<?php

require_once(__DIR__ . '/vendor/autoload.php');

use Spatie\ImageOptimizer\OptimizerChain;
use Spatie\ImageOptimizer\Optimizers\Svgo;
use Spatie\ImageOptimizer\Optimizers\Optipng;
use Spatie\ImageOptimizer\Optimizers\Gifsicle;
use Spatie\ImageOptimizer\Optimizers\Pngquant;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;

if(!class_exists('BravoWP_Image_Optimizer')) {

  class BravoWP_Image_Optimizer_Error extends Exception {
    public $status_code = 500;

    public function __construct($status_code, $message) {
      $this->status_code = $status_code;
      parent::__construct($message);
    }

  }

  class BravoWP_Image_Optimizer {
    public $root;
    public $upload;
    public $stats;
    public $cache;
    public $jpegQuality = 75;

    public function __construct($upload) {
      $debug = strpos($_SERVER['HTTP_HOST'], 'local.dev.bravomedia.se') !== false;
      $this->root = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';
      $this->upload = rtrim($upload, '/') . '/';
      $this->stats = $debug;
      $this->cache = !$debug;
    }

    public function optimize($relative_src) {
      if(strpos($relative_src, '..') !== false) {
        throw new BravoWP_Image_Optimizer_Error(403, 'Invalid src file');
      }
      $src = $this->root . ltrim($relative_src, '/');
      if(!is_file($src)) {
        throw new BravoWP_Image_Optimizer_Error(404, 'Source not found');
      }
      $ext = pathinfo($src, PATHINFO_EXTENSION);
      if($ext) {
        $ext = '.' . $ext;
      }
      $dst = $this->upload . 'bravo-image-cache/' . ltrim($relative_src, '/');
      $cache_dir = pathinfo($dst, PATHINFO_DIRNAME);
      if(!is_dir($cache_dir)) {
        mkdir($cache_dir, 0777, true);
      }
      if(!is_dir($cache_dir)) {
        throw new BravoWP_Image_Optimizer_Error(500, 'Invalid cache directory');
      }
      if($this->cache && is_file($dst)) {
        return $dst;
      }
      $optimizerChain = new OptimizerChain();
      $optimizerChain->setOptimizers([
        new Jpegoptim([
          '-m' . $this->jpegQuality,
          '--strip-all',
          '--all-progressive',
        ]),
        new Pngquant([
          '--force',
        ]),
        new Optipng([
          '-i0',
          '-o2',
          '-quiet',
        ]),
        new Svgo([
          '--disable=cleanupIDs',
        ]),
        new Gifsicle([
          '-b',
          '-O3',
        ]),
      ]);
      $optimizerChain->optimize($src, $dst);
      if(!is_file($dst)) {
        throw new BravoWP_Image_Optimizer_Error(500, 'Optimization failed');
      }
      if($this->stats) {
        $srcSize = filesize($src);
        $dstSize = filesize($dst);
        $ratio = ($srcSize - $dstSize) / $srcSize;
        error_log('IMAGE_OPTIMIZE: ' . sprintf('%03f', $ratio * 100) . '% - ' . basename($src) . ' ' . $srcSize . ' -> ' . $dstSize);
      }
      return $dst;
    }

    public function src() {
      if(isset($_GET['src'])) {
        return $_GET['src'];
      }
      if(isset($_SERVER['REDIRECT_URL'])) {
        return $_SERVER['REDIRECT_URL'];
      }
      throw new BravoWP_Image_Optimizer_Error(403);
    }

    public function run() {
      try {
        $src = $this->src();
        $dst = $this->optimize($src);
        header('Content-Type: ' . mime_content_type($dst));
        readfile($dst);
      } catch(BravoWP_Image_Optimizer_Error $error) {
        http_response_code($error->status_code);
        die($error->getMessage());
      }
    }
  }
}
