<?php

if(!defined('ABSPATH')) {
  exit;
}

if(isset($_GET['action'])) {
  switch($_GET['action']) {
  case 'enable':
    BravoWP_Image_Optimizer_Plugin::activate();
    break;
  case 'disable':
    BravoWP_Image_Optimizer_Plugin::deactivate();
    break;
  }
}

if(!is_file(__DIR__ . '/vendor/autoload.php')) {
  echo '<p style="color:red">Dependencies not installed, run composer install in plugin folder</p>';
}

if(!is_dir(BravoWP_Image_Optimizer_Plugin::getCacheDir())) {
  echo '<p style="color:red">Directory not found: ' . BravoWP_Image_Optimizer_Plugin::getCacheDir() . '</p>';
}

echo '<br>';

printf("<div style='margin-bottom:5px'>Environment supported: %s</div>", BravoWP_Image_Optimizer_Plugin::isSupporedEnvironment() ? '<span style="display:inline-block;padding:5px;background:green">YES</span>' : '<span style="display:inline-block;padding:5px;background:red">NO</span>');
printf("<div style='margin-bottom:5px'>Optimizer status: %s</div>", BravoWP_Image_Optimizer_Plugin::isActivated() ? '<span style="display:inline-block;padding:5px;background:green">ENABLED</span>' : '<span style="display:inline-block;padding:5px;background:red">DISABLED</span>');

printf("<a class='button' href='admin.php?page=bravo-image-optimizer&action=enable'>Enable optimizer</a>");
printf("<a class='button' href='admin.php?page=bravo-image-optimizer&action=disable'>Disable optimizer</a>");
